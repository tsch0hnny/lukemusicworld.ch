import { defineNuxtConfig } from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
    meta: {
        link: [
          { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
          { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
          { rel: 'preconnect', href: "https://app.snipcart.com" },
          { rel: 'preconnect', href: "https://cdn.snipcart.com" },
        //   { rel: 'stylesheet', href: "https://cdn.snipcart.com/themes/v3.2.1/default/snipcart.css" },
          // { rel: 'stylesheet', href: "https://cdn.snipcart.com/themes/v3.3.3/default/snipcart.css" },
          { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
          { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
          { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
          { rel: 'manifest', href: '/site.webmanifest' },
          { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#0e0e0e' },
          { name: 'msapplication-TileColor', content: '#0e0e0e' },
          { name: 'theme-color', content: '#0e0e0e' },
          { name: 'apple-mobile-web-app-capable', content: 'black-translucent' }
          
        ],
        script: [
            // { src: 'https://cdn.snipcart.com/themes/v3.2.1/default/snipcart.js', async: true }, // deprecated but keep it for backup
            // { src: 'https://cdn.snipcart.com/themes/v3.3.3/default/snipcart.js', async: true, defer: true },
            // { src: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js" },
            // { src: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/Flip.min.js" },
            { src: "https://www.googletagmanager.com/gtag/js?id=G-RLTMEEG1FS", async: true }
        ],
    }
})
