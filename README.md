# lukemusic.world Frontend (Nuxt 3)
This is the development branch of lukemusicworld.ch with the latest version of Nuxt 3 (Nuxt CLI v3.0.0-27495135.4d1a6ba)
Date of installation: 11.04.22

[documentation](https://v3.nuxtjs.org).

## Todo
Todos moved to Jira

https://web.dev/service-worker/?utm_source=lighthouse&utm_medium=devtools

## Setup

Make sure to install the dependencies

```bash
npm install
```

## Development

Start the development server on http://localhost:3000

```bash
npm run dev -- -o
```

## Production

Build the application for production:

```bash
npm run build
```

### preview in production mode

```bash
npm start
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/docs/deployment).